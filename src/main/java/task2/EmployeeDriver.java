package task2;

public class EmployeeDriver {
    public static void main(String[] args) {
        Employee employee1 = new Employee("Robert",1994,10000,"64C-WallsStreat");
        Employee employee2 = new Employee("Sam",2000,10000,"68D-WallsStreat");
        Employee employee3 = new Employee("John",1999,10000,"28B-WallsStreat");
        System.out.println(employee1.print());
        System.out.println(employee2.print());
        System.out.println(employee3.print());
    }
}
