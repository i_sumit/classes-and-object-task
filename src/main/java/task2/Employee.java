package task2;

public class Employee {
    private String name;
    private int yoj;
    private double salary;
    private String address;

    public Employee(String name, int yoj, double salary, String address) {
        this.name = name;
        this.yoj = yoj;
        this.salary = salary;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYoj() {
        return yoj;
    }

    public void setYoj(int yoj) {
        this.yoj = yoj;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String print(){
        return name + " " + yoj + " " + address;
    }
}
