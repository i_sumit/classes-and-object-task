package com.epam.task.task1;

public class BookMain {
    Book book1 = new Book(1,"Whispers in the Wind","Amelia Stone");
    Book book2= new Book(2,"Echoes of Eternity","Benjamin Rivers");
    Book book3 = new Book(3,"Shadows of the Past","Harper Montgomery");
    Book book4 = new Book(4,"Dancing with Destiny","Elliot Brooks");
    Book book5 = new Book(5,"Midnight Serenade","Clara Hartley");
    Book book6 = new Book(6,"Secrets in the Mist","Nathan Rivers");
    Book book7 = new Book(7,"Forgotten Dreams","Olivia Blake");
    Book book8 = new Book(8,"Sirens' Song","Dylan Knight");
    Book book9 = new Book(9,"Lost in the Echo","Maya Anderson");
    Book book10 = new Book(10,"Fragments of Forever","Ryan Carter");
}
