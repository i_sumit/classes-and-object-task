package task5;

import java.util.Arrays;

public class Sentence {
    public static void main(String[] args) {
        String sentence = " write a Java program to replace that word with a new one provided by the user";
        System.out.println(sentence.split(" ").length);
        sentence = replaceWord(sentence,"a","the word");
        System.out.println(sentence);
        System.out.println(totalWordsInSentence(sentence));
    }

    public static String replaceWord(String sentence, String oldWord, String newWord) {
        if(sentence.contains(oldWord)){
            String[] words = sentence.split(" ");
            for(int i = 0; i< words.length; i++) {
                if(words[i].equals(oldWord)) words[i] = newWord;
            }
            return Arrays.toString(words);
        }
        else throw new RuntimeException("Word not found in sentence");
    }

    public static int totalWordsInSentence(String sentence){
        return sentence.split(" ").length;
    }
}
