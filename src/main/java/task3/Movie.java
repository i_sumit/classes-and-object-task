package task3;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Movie {
    private String movieTitle;
    private String director;
    private int releaseYear;
    private String genre;
    private double averageRating;
    private int ratings;
    private List<String> reviews;

    public Movie(String movieTitle, String director, int releaseYear, String genre, double averageRating) {
        this.movieTitle = movieTitle;
        this.director = director;
        this.releaseYear = releaseYear;
        this.genre = genre;
        this.averageRating = averageRating;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    public void updateAverageRating(int newRating){
        ratings++;
        setAverageRating((averageRating * (ratings-1) + newRating)/ratings);
    }

    public void addReview(int rating,String review){
        updateAverageRating(rating);
        reviews.add(review);
    }

    public String getMovieDetails() {
        return "Movie{" +
                "movieTitle='" + movieTitle + '\'' +
                ", director='" + director + '\'' +
                ", releaseYear=" + releaseYear +
                ", genre='" + genre + '\'' +
                ", averageRating=" + averageRating +
                ", ratings=" + ratings +
                ", reviews=" + reviews +
                '}';
    }
}
